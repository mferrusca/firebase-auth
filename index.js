// capturing singup form
const signupform = document.querySelector('#signup-form');

// adding listener for data submition
signupform.addEventListener('submit', (e) => {
    // stopping defaults for testing
    e.preventDefault();
    console.log('submitting...')

    // capturing email input value
    const email = document.querySelector('#signup-email').value;
    // capturing password input value
    const password = document.querySelector('#signup-password').value;
    // printing values
    // console.log(signupemail, signuppassword);

    // FB propietary method
    auth
        .createUserWithEmailAndPassword(email, password)
        .then(userCredential => {
            // resetting modal
            signupform.reset();
            // closing modal
            $('#signupModal').modal('hide')
            // testing for data send
            console.log('sign up')
        })

});

// capturing login form
const loginform = document.querySelector('#login-form');
// listening for login submit button/event
loginform.addEventListener('submit', (e) => {
    // stopping defaults for login testing
    e.preventDefault();
    console.log('logging in...')
    // capturing login email input value
    const email = document.querySelector('#login-email').value;
    // capturing login password input value
    const password = document.querySelector('#login-password').value;
    // printing values
    console.log(email, password);
})